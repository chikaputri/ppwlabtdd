from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import status_Form, Subscribe_Form
from .models import status, Subscribe
from django.shortcuts import render
import requests

# Create your views here.
response={}

def index(request):
    response['stats'] = status_Form
    response['status'] = status.objects.all()
    html = 'index.html'
    return render(request, html, response)

def tambahstatus(request):
    form = status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['deskripsi'] = request.POST['deskripsi']
        a = status(deskripsi=response['deskripsi'])
        a.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

#------------------------------------------------------------------------------------------		

def profil(request):
	return render(request, 'profil.html')

#------------------------------------------------------------------------------------------

def book(request):
    return render(request, 'book.html')
    
def data_json(request):
    search_data = request.GET.get("search_field")
    search = "https://www.googleapis.com/books/v1/volumes?q=" + str(search_data)
    my_list = requests.get(search)
    json = my_list.json()
    return JsonResponse(json)

#-------------------------------------------------------------------------------------------

def subscribe(request):
    forms = Subscribe.objects.all()
    response = {'form' : Subscribe_Form, 'result': forms}
    return render(request, 'subscribe.html', response)

def add_subscriber(request):
    response = {}
    if (request.method == "POST" or None):
        response['email'] = request.POST['email']
        response['nama'] = request.POST['nama']
        response['password'] = request.POST['password']
        subscriber = Subscribe(email = response['email'], nama = response['nama'], password = response['password'])
        subscriber.save()
    return JsonResponse({})

def check_valid(request):
    email = request.POST.get('email', None)
    data = {
        'is_taken': Subscribe.objects.filter(email=email).exists()
    }
    return JsonResponse(data)

#------------------------------------------------------------------------------------------     

def login(request):
    return render(request, 'login.html')

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username = username, password = password)
    if user is not None:
        request.session['user_login'] = username
        res =  HttpResponseRedirect('status:book')
        res.set_cookie('test', 'berhasil')
        res.set_cookie('username', username)
    else:
        return HttpResponseRedirect('/login')
        
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/book')


