from django.conf import settings
from django.db import models
from django.core.validators import MinLengthValidator

# Create your models here.
class status(models.Model):
	deskripsi = models.TextField(max_length=300)
	tanggal = models.DateTimeField(auto_now_add=True)

class Subscribe(models.Model):
	email = models.EmailField(error_messages={'unique': 'An account with this email exist.'})
	nama = models.CharField(max_length = 50)
	password = models.CharField(validators=[MinLengthValidator(4)], max_length = 20)