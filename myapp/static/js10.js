function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

var is_unique = false;
$(function() {
    $("#id_email").change(function() {
        console.log( $(this).val() );
        email = $(this).val();
    console.log("hehehehe");
        $.ajax({
            method: "POST",
            url: '{% url "check_valid/" %}',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)
                if (data.is_taken) {
                    alert("Email sudah terdaftar sebelumnya, silakan pakai email lain");
                    is_unique = false;
                }
                else{
                    is_unique = true;
                }
            }
        });
    });
    $('#id_email').on('keyup', function () {
        check_submit();
    });
    $('#id_nama').on('keyup', function () {
        check_submit();
    });
    $('#id_password').on('keyup', function () {
        check_submit();
    });
});

function create_post() {
    $.ajax({
        url : "add_subscriber/", // the endpoint
        type : "POST", // http method
        data : { 
            email : $('#id_email').val(), 
            nama : $('#id_nama').val(), 
            password : $('#id_password').val() }, // data sent with the post request
        success : function(json) {
            $('#id_email').val(''); // remove the value from the input
            $('#id_nama').val('');
            $('#id_password').val('');
        },
    });
};

function check_submit(){
    var panjang_nama = $('#id_nama').val().length;
    var panjang_email = $('#id_email').val().length;
    var panjang_password = $('#id_password').val().length;

    if(panjang_nama > 0 && panjang_email > 0 && panjang_password > 0 && is_unique) {
        $('#submit_regis').prop('disabled', false);
    } else {
        $('#submit_regis').prop('disabled', true);
    }
}

function submit_post() {
    $.ajax({
        url : "/add_subscriber/", // the endpoint
        type : "POST", // http method
        data : { nama : $('#id_nama').val(), email : $('#id_email').val(), password : $('#id_password').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            $('#id_nama').val(''); // remove the value from the input
            $('#id_email').val(''); // remove the value from the input
            $('#id_password').val(''); // remove the value from the input
            $('#submit_regis').prop('disabled', true);
        },
    });
}