$(document).ready(function(){
		$("h1").bind("mouseover",function(){
			var a = $(this).css("background-color");
			var b = $(this).css("font-color");

			$(this).css("background", "black");
			$(this).css("font-color", "white");

			$(this).bind("mouseout", function(){
				$(this).css("background", a);
				$(this).css("font-color", b);
			})
		})
	})
	$(document).ready(function(){
		$("#kotak").click(function(){
			$("#panel").slideToggle("slow");
		})
	})
	$(document).ready(function(){
		$("#kotak2").click(function(){
			$("#panel2").slideToggle("slow");
		})
	})
	$(document).ready(function(){
		$("#kontak3").click(function(){
			$("#panel3").slideToggle("slow");
		})
	})
	$(document).ready(function(){
		$('.my-select').select2();
	})

	$(document).ready(function() {
    $("button").click(function(){
        var c = $("body").css("background-color");
        var d = $(".jumbotron").css("background-color");
        var f = $("h1").css("background-color");

        $("body").css("background", "#FCB9E3");
        $(".jumbotron").css("background", "rgb(91,192,222)", "!important");
        $("h1").css("background", "#E77ABE");

        $("button").click(function(){
            $("body").css("background-color", c);
            $(".jumbotron").css("background-color", d);
            $("h1").css("background-color", f);
        })    
    })    
})

var counter = 0;
function changeStar(id){
    var star = $('#'+id).html();
    if(star.includes("gray")) {
        counter++;
        $('#'+id).html("<i class='fa fa-star' style = 'color : yellow'></i>");
        $("#jumlahBuku").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " ");
    }
    else{
        counter--;
        $('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
        $("#jumlahBuku").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " ");
    }   
}


$(document).ready(function(){
    $.ajax({
        url: "/data_json",
        success: function(obj){
            obj = obj.items;
            $("#tableBuku").append("<thead><th> Title </th> <th> Author </th> <th> Cover </th> <th> Description </th><th> Favorite </th> </thead>");
            $("#tableBuku").append("<tbody>");
            for(c = 0; c < obj.length; c++){
                var title = obj[c].volumeInfo.title;
                var author = obj[c].volumeInfo.authors;
                var image = obj[c].volumeInfo.imageLinks.thumbnail;
                var description = obj[c].volumeInfo.description;
                $("#tableBuku").append("<tr><td>" + title +  "</td> <td> "+ author + "</td> <td><img src = '" + image + "'</img></td> <td>" + description + "</td> <td>" + 
                                    "<button class='button' style = 'background-color: Transparent; border: none' id='"+ obj[c].id + 
                                    "'onclick = 'changeStar(" +"\""+ obj[c].id+"\""+")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>");
            }
            $("#tableBuku").append("</tbody>");
        }
    });
});

$(function() {
  $("#accordion").accordion({
    collapsible: true
  });
});

var isEmailValidGlobal = false;
var timer = null;
$('#emailInput').keydown(function() {
  formComplete();
  $("#emailDiv small").html("");
  clearTimeout(timer);
  timer = setTimeout(checkEmail, 500)
  formComplete()
});

$("#nameInput").on("input", function() {
  $("#emailDiv small").html("");
  formComplete();
  clearTimeout(timer);
  timer = setTimeout(checkEmail, 500)
});

$("#passwordInput").on("input", function() {
  $("#emailDiv small").html("");
  formComplete();
  clearTimeout(timer);
  timer = setTimeout(checkEmail, 500)
});

function checkEmail() {
  $("#emailDiv small").html("");

  data = {
    "inputValue": $("#emailInput").val(),
    "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
  }

  $.ajax({
    type: "POST",
    url: "check-email/",
    data: data,
    dataType: "json"
  })
  .done(isEmailAvailabe)
  .fail(function(res) {
    console.log(res)
  });
}

function isEmailAvailabe(data) {
  $("#emailDiv small").html("");
  console.log(data)
  if (data['status_code'] === 400) {
    $("#emailDiv").append(`<small class='error'>${data['message']}</small>`);
    isEmailValidGlobal = false;
    formComplete();
  } else {
    console.log(data)
    $("#emailDiv").append(`<small class='success'>${data['message']}</small>`);
    isEmailValidGlobal = true;
    formComplete();
  }
}

function formComplete() {
  let isEmailValid = ($("#emailInput").val() !== "") ? true : false;
  let isPasswordValid = ($("#passwordInput").val() !== "") ? true : false;
  let isNameValid = ($("#nameInput").val() !== "") ? true : false;
  let isPassLengthValid = ($("#passwordInput").val().length > 6) ? true : false;

  if ($("#emailInput").val() !== "" && $("#passwordInput").val() !== "" &&
    $("#nameInput").val() !== "" && $("#passwordInput").val().length > 6 && isEmailValidGlobal) {
    $('#submitButton').prop('disabled', false);
  } else {
    $('#submitButton').prop('disabled', true);
  }
}

function subs() {
  data = {
    "name": $("#nameInput").val(),
    "email": $("#emailInput").val(),
    "password": $("#passwordInput").val(),
    "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
  }
  $.ajax({
    type:"POST",
    url: "subscribe/",
    data: data,
    dataType: "json",
  })
    .done(function(data) {
      if (data.status_code === 400) {
        alert(data.message);
      } else {
        alert("Subscribed!");
      }
    })
    .fail(function(res) {
      alert(res)
    })
}