from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, tambahstatus, profil
from .models import status
from .forms import status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class story6UnitTest(TestCase):
    
    def test_myapp_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_myapp_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_model_can_fill_status(self):
    	new_status = status.objects.create(deskripsi='Hai')
    	counting_all_available_status = status.objects.all().count()
    	self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = status_Form(data={'deskripsi': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['deskripsi'], ["This field is required."])

    def test_myapp_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/tambahstatus', {'deskripsi': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab5_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/tambahstatus', {'deskripsi': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_challenge6_url_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code,200)

    def test_challenge6_using_profil_func(self):
        found = resolve('/profil/')
        self.assertEqual(found.func, profil)

    def test_name_in_html(self):
        response = Client().get('/profil/')
        html_response = response.content.decode('utf8')
        self.assertIn("Chika Putri", html_response)

# Create your tests here.
class story6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver.exe',         chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(story6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        deskripsi = selenium.find_element_by_id('id_deskripsi')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        deskripsi.send_keys('Selenium Test')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        
    def test_index_layout(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        
        self.assertIn('Story6_GracielaChikaPutri', selenium.title)
        header_text = selenium.find_element_by_tag_name('h1').text
        self.assertIn('Hello, Apa kabar?', header_text)
        
    def test_index_css(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        
        main_title = selenium.find_element_by_css_selector('h1.main-title')
        
        container = selenium.find_element_by_css_selector('div.container')
        
        button = selenium.find_element_by_css_selector('input.btn btn-lg btn-block btn-info')
        
        table = selenium.find_element_by_css_selector('table.table')
    
    def test_profil_layout(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/profil.html')
        self.assertIn('Chika Putri', selenium.title)
        header_text = selenium.find_element_by_tag_name('h1').text
        self.assertIn('Profil', header_text)
    
    def test_theme_is_changed(self):
        self.browser.get('http://127.0.0.1:8000/profil.html')
        time.sleep(3)
        old_theme = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
        tombol = self.browser.find_element_by_name('ubah_tema')
        tombol.send_keys(Keys.RETURN)
        new_theme = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
        time.sleep(3)
        self.assertNotEqual(old_theme, new_theme);

    def test_panel_is_showed_and_closed(self):
        self.browser.get('http://127.0.0.1:8000/profil.html')
        time.sleep(3)
        acc = self.browser.find_element_by_tag_name("h1");
        acc.click()
        self.assertTrue("closed" not in acc.get_attribute("class"))
        time.sleep(3)
        acc.click()
        self.assertTrue("closed" in acc.get_attribute("class"))
        time.sleep(3)
