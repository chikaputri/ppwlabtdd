from django import forms

class status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    deskripsi_attrs = {
        'type': 'text',
        'placeholder':'Apa yang anda pikirkan...'
    }

    deskripsi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=deskripsi_attrs))

class Subscribe_Form(forms.Form):
	nama_attrs = {
		'id' : 'id_nama',
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Name'
    }
	email_attrs = {
		'id' : 'id_email',
        'type': 'email',
        'class': 'form-control',
        'placeholder':'Email'
    }
	pass_attrs = {
		'id' : 'id_password',
        'type': 'password',
        'class': 'form-control',
        'placeholder':'Password',
		'required pattern' : '[a-zA-Z0-9]+',
		'title' : 'Harus Alpha Numeric'
    }
	email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
	nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
	password = forms.CharField(min_length = 8 ,widget=forms.TextInput(attrs=pass_attrs))