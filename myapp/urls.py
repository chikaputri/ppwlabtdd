from django.conf.urls import url, include
from .views import index, tambahstatus, profil, book, subscribe, data_json, add_subscriber, check_valid, login, logout_view

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'tambahstatus', tambahstatus, name='tambahstatus'),
    url(r'^profil', profil, name = 'profil'),
    url(r'^book', book, name = 'book'),
    url(r'^subscribe', subscribe, name='subscribe'),
    url(r'^data_json', data_json, name = 'data_json'),
    url(r'^check_valid/', check_valid, name='check_valid'),
    url(r'^add_subscriber/', add_subscriber, name='add_subscriber'),
    url(r'^login', login, name='login'),
    url(r'^logout/', logout_view, name='logout'),

]